from pprint import pprint

from PyInstaller.utils.hooks import collect_data_files, collect_submodules

package_name = "SWParser"
datas = collect_data_files(package_name)
hiddenimports = collect_submodules(package_name)

pprint(f"datas: {datas}")
pprint(f"hiddenimports: {hiddenimports}")
